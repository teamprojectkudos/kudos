package com.kudos.kudosneo4j.controller;

import com.kudos.kudosneo4j.domain.Kudo;
import com.kudos.kudosneo4j.domain.Person;
import com.kudos.kudosneo4j.exception.ResourceNotFoundException;
import com.kudos.kudosneo4j.services.KudoService;
import com.kudos.kudosneo4j.services.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by ALEXIS ARDAYA on 30/7/2018.
 */
@RestController
@RequestMapping("/kudo")
public class KudoController {

    private final KudoService kudoService;
    private final PersonService personService;

    public KudoController(KudoService kudoService, PersonService personService) {
        this.kudoService = kudoService;
        this.personService = personService;
    }

    @PostMapping(value = "/save")
    public ResponseEntity<Void> save(@RequestBody Kudo kudo) throws URISyntaxException {
        Kudo newKudo = kudoService.save(kudo);
        return ResponseEntity.created(new URI("/kudo/" + newKudo.getId())).build();
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            kudoService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path="")
    public ResponseEntity<List<Kudo>> findAll() {
        List<Kudo> kudos = kudoService.findAll();
        return ResponseEntity.ok(kudos);
    }
}
