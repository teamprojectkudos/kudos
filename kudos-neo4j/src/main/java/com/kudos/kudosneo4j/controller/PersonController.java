package com.kudos.kudosneo4j.controller;

import com.kudos.kudosneo4j.domain.Kudo;
import com.kudos.kudosneo4j.domain.Person;
import com.kudos.kudosneo4j.exception.ResourceNotFoundException;
import com.kudos.kudosneo4j.services.KudoService;
import com.kudos.kudosneo4j.services.PersonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by ALEXIS ARDAYA on 30/7/2018.
 */
@RestController
@RequestMapping("/person")
public class PersonController {

    private final PersonService personService;
    private final KudoService kudoService;

    public PersonController(PersonService personService, KudoService kudoService) {
        this.personService = personService;
        this.kudoService = kudoService;
    }

    @PostMapping("/save")
    public ResponseEntity<Void> save(@RequestBody Person person) throws URISyntaxException {
        Person newPerson = personService.save(person);
        return ResponseEntity.created(new URI("/person/" + newPerson.getId())).build();
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            personService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/graph")
    public Map<String, Object> graph(@RequestParam(value = "limit", required = false) Integer limit) {
        return personService.graph(limit == null ? 100 : limit);
    }

    @GetMapping(path="/{userId}/kudos")
    public  ResponseEntity<List<Person>> getKudosReceivedByUser(@PathVariable Long userId) {
        List<Person> persons = personService.kudosReceivedByPersons(userId);
        return ResponseEntity.ok(persons);
    }

    @GetMapping(path="/{userId}")
    public ResponseEntity<Optional<Optional<Person>>> getPersonById(@PathVariable Long userId) {
        try {
            Optional<Person> person = personService.finById(userId);
            return ResponseEntity.ok(Optional.ofNullable(person));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(path="/allpersons")
    public ResponseEntity<Model> getAll(Model model){
        try {
            Model persons = model.addAttribute("persons", personService.findAllPerson());
            return ResponseEntity.ok(persons);
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(path="/deleteKudosByUser")
    public ResponseEntity<Object> deleteKudosByUser(@PathVariable Long id) {
        Optional<Person> person = personService.finById(id);
        List<Kudo> kudos;
        if (person.isPresent()){
            kudos = person.get().getKudos();
            kudos.stream().forEach(kudo -> kudoService.deleteById(kudo.getId()));
            return ResponseEntity.ok(kudos);
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
