package com.kudos.kudosneo4j.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by ALEXIS ARDAYA on 30/7/2018.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@NodeEntity
public class Person {
    @Id
    @GeneratedValue
    private Long id;
    private String idMongo;

    @Relationship(type = "KUDO", direction =  Relationship.INCOMING)
    @JsonBackReference
    private List<Kudo> kudos = new ArrayList<>();

    public List<Kudo> getKudos() {
        return kudos;
    }

    public void addKudo(Kudo kudo) {
        if (this.kudos == null) {
            this.kudos = new ArrayList<>();
        }
        this.kudos.add(kudo);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(getId(), person.getId()) &&
                Objects.equals(getIdMongo(), person.getIdMongo()) &&
                Objects.equals(getKudos(), person.getKudos());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getIdMongo(), getKudos());
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", idMongo="+ idMongo+
                ", kudos=" + kudos +
                '}';
    }
}
