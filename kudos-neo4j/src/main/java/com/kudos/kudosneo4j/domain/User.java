package com.kudos.kudosneo4j.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String idMongo;
    private String nickname;
    private String realName;
    private Integer kudosQty;
}
