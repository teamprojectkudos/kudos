package com.kudos.kudosneo4j.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;
import org.springframework.stereotype.Component;

/**
 * Created by ALEXIS ARDAYA on 30/7/2018.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RelationshipEntity(type = "KUDO")
public class Kudo {
    @Id
    @GeneratedValue
    private Long id;
    private String category;
    private String description;
    private String place;

    @StartNode
    private Person source;

    @EndNode
    private Person target;

    public Kudo(Person source, Person target) {
        this.source = source;
        this.target = target;
    }
}
