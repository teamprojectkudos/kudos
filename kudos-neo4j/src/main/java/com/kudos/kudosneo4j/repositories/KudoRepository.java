package com.kudos.kudosneo4j.repositories;

import com.kudos.kudosneo4j.domain.Kudo;
import com.kudos.kudosneo4j.domain.Person;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;


/**
 * Created by ALEXIS ARDAYA on 30/7/2018.
 */
public interface KudoRepository extends Neo4jRepository<Kudo, Long> {
    @Query("MATCH (s:Person)-[r:HAS]-(t:Person) RETURN s,r,t")
    Collection<Kudo> kudosWithDetail();
}
