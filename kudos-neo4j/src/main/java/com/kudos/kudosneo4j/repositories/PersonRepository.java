package com.kudos.kudosneo4j.repositories;

import com.kudos.kudosneo4j.domain.Person;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by ALEXIS ARDAYA on 30/7/2018.
 */
public interface PersonRepository extends Neo4jRepository<Person, Long> {
    @Query("MATCH (p:Person) WHERE ID(p)={id} return p")
    Optional<Person> findById(@Param("id") Long id);

    @Query("MATCH (p:Person) WHERE ID(p)={id} return p.idMongo")
    String findByIdMongo(@Param("id") Long id);

    @Query("MATCH (s:Person)<-[r:KUDO]-(t:Person) RETURN s,r,t LIMIT {limit}")
    Collection<Person> graph(@Param("limit") int limit);

    @Query("MATCH (s:Person)<-[r:KUDO]-(t:Person) WHERE ID(s)={id} RETURN s,r,t")
    Collection<Person> kudosReceivedByPerson(@Param("id") Long id);
}
