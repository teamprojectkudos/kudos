package com.kudos.kudosneo4j.services;

import com.google.gson.Gson;
import com.kudos.kudosneo4j.KudosNeo4jApplication;
import com.kudos.kudosneo4j.custom_json.CustomMessage;
import com.kudos.kudosneo4j.custom_json.MessageBody;
import com.kudos.kudosneo4j.domain.Kudo;
import com.kudos.kudosneo4j.domain.Person;
import com.kudos.kudosneo4j.repositories.KudoRepository;
import com.kudos.kudosneo4j.repositories.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * Created by ALEXIS ARDAYA on 30/7/2018.
 */
@Service
public class KudoService {
    private final static Logger LOG = LoggerFactory.getLogger(KudoService.class);

    private final KudoRepository kudoRepository;
    private final PersonRepository personRepository;
    private RabbitTemplate rabbitTemplate;

    public KudoService(KudoRepository kudoRepository, PersonRepository personRepository, RabbitTemplate rabbitTemplate){
        this.kudoRepository = kudoRepository;
        this.personRepository = personRepository;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Transactional
    public List<Kudo> findAll() {
        return (List<Kudo>) kudoRepository.kudosWithDetail();
    }

    @Transactional
    public Kudo save(Kudo kudo) {
        String detail = "Creado "+kudo.getCategory()+" de usuario "+ kudo.getSource().getId() +" a "+kudo.getTarget().getId();
        String customLog = customLog("Create Kudo",detail);
        LOG.info(customLog);
        kudoRepository.save(kudo);
        Person person = personRepository.findById(kudo.getTarget().getId()).orElse(null);
        person.addKudo(kudo);
        personRepository.save(person);
        Integer kudos = kudo.getTarget().getKudos().size();
        LOG.info("kudos por usuario", kudos);
        sendKudoMessage(kudo.getTarget().getIdMongo(), kudos);
        return kudo;
    }

    @Transactional
    public void deleteById(Long id) {
        String detail = "Kudo "+id+" was deleted";
        String customLog = customLog("Delete Kudo",detail);
        LOG.info(customLog);
        kudoRepository.deleteById(id);
    }

    public static String convert(CustomMessage customMessage) {
        Gson gson = new Gson();
        String json = gson.toJson(customMessage);
        return json;
    }

    public void sendKudoMessage(String id, Integer kudosQty) {
        MessageBody messageBody = new MessageBody();
        messageBody.setId(id);
        messageBody.setKudosQty(kudosQty);
        CustomMessage customMessage = new CustomMessage();
        customMessage.setType("user");
        customMessage.setModel("User");
        customMessage.setMessageBody(messageBody);
        LOG.info("Sending request ", customMessage);
        rabbitTemplate.convertAndSend(KudosNeo4jApplication.SFG_MESSAGE_QUEUE, convert(customMessage));
    }

    public String customLog(String event,String detail){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return String.format("[%s] [%s] %s",dateFormat.format(date),event,detail);
    }
}
