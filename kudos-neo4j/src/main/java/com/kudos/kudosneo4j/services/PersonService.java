package com.kudos.kudosneo4j.services;

import com.kudos.kudosneo4j.domain.Kudo;
import com.kudos.kudosneo4j.domain.Person;
import com.kudos.kudosneo4j.domain.User;
import com.kudos.kudosneo4j.repositories.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ALEXIS ARDAYA on 30/7/2018.
 */
@Service
public class PersonService {
    private final static Logger LOG = LoggerFactory.getLogger(PersonService.class);

    private final RestTemplate restTemplate;
    private final String GET_ALL_URL = "http://192.168.0.122:9191/api/users";
    private final String GET_URL_BY_ID = "http://localhost:8080/api/product/";

    private final PersonRepository personRepository;

    public PersonService(RestTemplate restTemplate, PersonRepository personRepository) {
        this.restTemplate = restTemplate;
        this.personRepository = personRepository;
    }

    private Map<String, Object> toD3Format(Collection<Person> persons) {
        List<Map<String, Object>> nodes = new ArrayList<>();
        List<Map<String, Object>> rels = new ArrayList<>();
        int i = 0;
        for (Person person : persons) {
            nodes.add(map("id", person.getId(), "label", "source"));
            int target = i;
            i++;
            for (Kudo kudo : person.getKudos()) {
                Map<String, Object> kudo_source = map(
                        "id", kudo.getSource(),
                        "category", kudo.getCategory()
                );
                int source = nodes.indexOf(kudo_source);
                if (source == -1) {
                    nodes.add(kudo_source);
                    source = i++;
                }
                rels.add(map("source", source, "target", target));
            }
        }
        return map("nodes", nodes, "kudos", rels);
    }

    private Map<String, Object> map(String key1, Object value1, String key2, Object value2) {
        Map<String, Object> result = new HashMap<String, Object>(2);
        result.put(key1, value1);
        result.put(key2, value2);
        return result;
    }

    @Transactional(readOnly = true)
    public Map<String, Object> graph(int limit) {
        Collection<Person> result = personRepository.graph(limit);
        return toD3Format(result);
    }

    @Transactional
    public Person save(Person person) {
        personRepository.save(person);
        return person;
    }

    @Transactional
    public void deleteById(Long id) {
        personRepository.deleteById(id);
    }

    @Transactional
    public List<Person> kudosReceivedByPersons(Long userId) {
        Collection<Person> persons = personRepository.kudosReceivedByPerson(userId);
        return persons.stream().filter(person -> person.getKudos().size() > 0).collect(Collectors.toList());
    }

    @Transactional
    public Optional<Person> finById(Long userId) {
        Optional<Person> person = personRepository.findById(userId);
        return person;
    }

    public String customLog(String event,String detail){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return String.format("[%s] [%s] %s",dateFormat.format(date),event,detail);
    }

    public List<User> findAllPerson(){
        return Arrays.stream(restTemplate.getForObject(GET_ALL_URL, User[].class)).collect(Collectors.toList());
    }

    public String findByIdMongo(Long id){
        return personRepository.findByIdMongo(id);
    }
}
