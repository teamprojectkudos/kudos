
package com.kudos.kudosneo4j.exception;

/**
 * Created by ALEXIS ARDAYA on 30/7/2018.
 */
public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private Long id;

    public ResourceNotFoundException(Long id) {
        this.id = id;
    }

    public ResourceNotFoundException(){

    }
    public Long getId() {
        return id;
    }
}
