package com.kudos.kudosneo4j.custom_json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomMessage {
    private String type;
    private String model;
    private MessageBody messageBody;
}

