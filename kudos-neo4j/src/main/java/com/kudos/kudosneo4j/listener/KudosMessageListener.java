package com.kudos.kudosneo4j.listener;

import com.kudos.kudosneo4j.domain.Kudo;
import com.kudos.kudosneo4j.repositories.KudoRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * This is the queue listener class, its receiveMessage() method ios invoked with the
 * message as the parameter.
 */
@Component
public class KudosMessageListener {

    private KudoRepository productRepository;

    private static final Logger log = LogManager.getLogger(KudosMessageListener.class);

    public KudosMessageListener(KudoRepository productRepository) {
        this.productRepository = productRepository;
    }


    public void receiveMessage(String message) {
        log.info("Received <" + message + ">");
//        Long id = Long.valueOf(message.get("id"));
//        Kudo product = productRepository.findById(id).orElse(null);
//        log.info("Message processed...", product);
    }
}
