package org.kudos.resources.kudosmessage;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.kudos.api.KudosData;
import org.kudos.core.CustomMessage;

public class KudosMessageServiceImpl implements KudosMessageService {
    @Override
    public void sendNewKudos(CustomMessage customMessage) throws UnirestException {
        HttpResponse<JsonNode> jsonResponse = Unirest.post("http://localhost:9090/api/kudos_message")
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .body(customMessage)
                .asJson();
    }
}
