package org.kudos.resources.kudosmessage;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.kudos.api.KudosData;
import org.kudos.core.CustomMessage;

public interface KudosMessageService {
    void sendNewKudos(CustomMessage customMessage) throws UnirestException;
}
