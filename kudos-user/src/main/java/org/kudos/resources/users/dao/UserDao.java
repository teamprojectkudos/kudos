package org.kudos.resources.users.dao;

import org.kudos.api.User;

import java.util.List;
import java.util.Optional;

public interface UserDao {
    long create(User user);
    void delete(String userId);
    Optional<User> getById(long userId);

    Optional getById(String userId);

    List<User> getUsers();
    List<User> searchUsersByNickName(String nickName);
    List<User> serachUsersByRealName(String realname);

    void update(User user);
}
