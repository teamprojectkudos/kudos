package org.kudos.resources.users;

import com.codahale.metrics.annotation.Timed;
import org.kudos.api.User;
import org.kudos.core.Utils;
import org.kudos.resources.users.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path(UserResource.NAME)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    public static final String NAME = "users";
    private final UserService userService;

    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @GET
    @Timed
    public Response getAll() {
        return Response.ok(userService.getUsers()).build();
    }

    @GET
    @Path("{id}")
    @Timed
    public Response getById(@PathParam("id") String userId) {
        Object object = userService.getById(userId);
        if (object != null) {
            return Response.ok(userService.getById(userId)).build();
        } else {
            return Response.noContent().build();
        }
    }

    @POST
    @Timed
    public Response create(User user) {
        long id = userService.create(user);
        String resourcePath = Utils.getCreatedUrl(UserResource.NAME, id);
        return Response.created(URI.create(resourcePath)).build();
    }

    @DELETE
    @Path("{id}")
    @Timed
    public Response delete(@PathParam("id") String userId) {
        userService.delete(userId);
        return Response.ok().build();
    }
}
