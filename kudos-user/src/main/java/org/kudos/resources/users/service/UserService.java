package org.kudos.resources.users.service;

import org.kudos.api.User;

import java.util.List;

public interface UserService {
    long create(User user);
    void delete(String userId);
    List<User> getUsers();
    List<User> searchUsersByNickName(String nickName);
    List<User> serachUsersByRealName(String realname);

    Object getById(String userId);
}
