package org.kudos.resources.users.service;

import org.kudos.api.User;
import org.kudos.resources.users.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class UserServiceImpl implements UserService {

    private final static Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public long create(User user) {
        return userDao.create(user);
    }

    @Override
    public void delete(String userId) {
        userDao.delete(userId);
    }

    @Override
    public List<User> getUsers() {
        String detail = "Get all Users";
        String customLog = customLog("Get All", detail);
        LOG.info(customLog);

        return userDao.getUsers();
    }

    @Override
    public List<User> searchUsersByNickName(String nickName) {
        return userDao.searchUsersByNickName(nickName);
    }

    @Override
    public List<User> serachUsersByRealName(String realname) {
        return userDao.serachUsersByRealName(realname);
    }

    @Override
    public Object getById(String userId) {
        return userDao.getById(userId);
    }

    public String customLog(String event,String detail){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return String.format("[%s] [%s] %s",dateFormat.format(date),event,detail);
    }
}
