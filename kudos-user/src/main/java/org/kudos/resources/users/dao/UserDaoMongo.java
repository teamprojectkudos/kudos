package org.kudos.resources.users.dao;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import org.bson.BSONObject;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.kudos.api.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;

public class UserDaoMongo implements UserDao {

    private final MongoCollection<Document> collection;

    public UserDaoMongo(MongoCollection<Document> collection) {
        this.collection = collection;
    }

    @Override
    public long create(User user) {

        Gson gson = new Gson();
        String json = gson.toJson(user);
        collection.insertOne(new Document(BasicDBObject.parse(json)));
        return 0;
    }

    @Override
    public void delete(String userId) {
        collection.deleteOne(eq("_id", userId));
    }

    @Override
    public Optional<User> getById(long userId) {
        return Optional.empty();
    }

    @Override
    public Optional getById(String userId) {
        return Optional.ofNullable(collection.find(eq("_id", new ObjectId(userId))).map(value -> new User(value.get("_id").toString(), value.get("nickname").toString(), value.get("realName").toString(), Integer.valueOf(value.get("kudosQty").toString()))).first());
    }

    @Override
    public List getUsers() {
        return collection.find().map(value -> new User(value.get("_id").toString(), value.get("nickname").toString(), value.get("realName").toString(), Integer.valueOf(value.get("kudosQty").toString()))).into(new ArrayList<>());
    }

    @Override
    public List<User> searchUsersByNickName(String nickName) {
        return null;
    }

    @Override
    public List<User> serachUsersByRealName(String realname) {
        return null;
    }

    @Override
    public void update(User user) {
        Bson newValue = new Document("kudosQty", user.getKudosQty());
        Bson updateOperationDocument = new Document("$set", newValue);
        collection.updateOne(eq("_id", new ObjectId(user.getIdMongo())), updateOperationDocument);

    }
}
