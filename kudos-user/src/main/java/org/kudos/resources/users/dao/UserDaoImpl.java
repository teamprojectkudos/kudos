//package org.kudos.resources.users.dao;
//
//import io.dropwizard.hibernate.AbstractDAO;
//import org.hibernate.SessionFactory;
//import org.kudos.api.User;
//
//import javax.inject.Inject;
//import java.util.List;
//import java.util.Optional;
//
//public class UserDaoImpl extends AbstractDAO<User> implements UserDao {
//
//    @Inject
//    public UserDaoImpl(SessionFactory sessionFactory) {
//        super(sessionFactory);
//    }
//
//    @Override
//    public long create(User user) {
//        return persist(user).getId();
//    }
//
//    @Override
//    public void delete(User user) {
//        currentSession().delete(user);
//    }
//
//    @Override
//    public Optional<User> getById(long userId) {
//        return Optional.ofNullable(get(userId));
//    }
//
//    @Override
//    public List<User> getUsers() {
//        return list(currentSession().createQuery("SELECT u FROM User u", User.class));
//    }
//
//    @Override
//    public List<User> searchUsersByNickName(String nickName) {
//        return list(currentSession().createQuery("SELECT u FROM User u WHERE u.nickName LIKE :nickname")
//                .setParameter("nickname", "%" + nickName + "%"));
//    }
//
//    @Override
//    public List<User> serachUsersByRealName(String realname) {
//        return list(currentSession().createQuery("SELECT u FROM User u WHERE u.realName LIKE :realname")
//                .setParameter("realname", "%" + realname + "%"));
//    }
//
//    @Override
//    public void update(User user) {
//        persist(user);
//    }
//}
