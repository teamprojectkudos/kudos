package org.kudos.resources.kudosmessage.service;

import org.kudos.api.KudoData;
import org.kudos.core.CustomMessage;

public interface KudosMessageService {
    void kudosCreated(CustomMessage customMessage) throws Throwable;
}
