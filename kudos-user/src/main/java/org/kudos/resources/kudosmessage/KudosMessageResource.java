package org.kudos.resources.kudosmessage;

import org.kudos.api.KudoData;
import org.kudos.core.CustomMessage;
import org.kudos.resources.kudosmessage.service.KudosMessageService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(KudosMessageResource.NAME)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class KudosMessageResource {
    public static final String NAME = "kudos_message";
    private final KudosMessageService kudosMessageService;

    public KudosMessageResource(KudosMessageService kudosMessageService) {
        this.kudosMessageService = kudosMessageService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(CustomMessage customMessage) throws Throwable {
        kudosMessageService.kudosCreated(customMessage);
        return Response.ok().build();
    }
}
