package org.kudos.resources.kudosmessage.service;

import org.kudos.api.User;
import org.kudos.core.CustomMessage;
import org.kudos.resources.users.dao.UserDao;

import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

public class KudosMessageServiceImpl implements KudosMessageService {

    private final UserDao userDao;

    @Inject
    public KudosMessageServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void kudosCreated(CustomMessage customMessage) throws Throwable {
        User user = (User) userDao.getById(customMessage.getMessageBody().getId()).orElseThrow(() -> new NotFoundException("users"));

        user.setKudosQty(user.getKudosQty() + customMessage.getMessageBody().getKudosQty());
        userDao.update(user);
    }
}
