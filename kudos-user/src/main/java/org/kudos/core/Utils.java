package org.kudos.core;

public interface Utils {
    static String getCreatedUrl(String path, long id) {
        return String.format("./%s/%d", path, id);
    }
}
