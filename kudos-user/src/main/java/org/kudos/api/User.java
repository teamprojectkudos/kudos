package org.kudos.api;

import lombok.Data;


@Data
public class User {

    private Long id;

    private String idMongo;

    private String nickname;

    private String realName;

    private int kudosQty;

    public User() {
    }

    public User(String idMongo, String nickname, String realName, int kudosQty) {
        this.idMongo = idMongo;
        this.nickname = nickname;
        this.realName = realName;
        this.kudosQty = kudosQty;
    }
}

