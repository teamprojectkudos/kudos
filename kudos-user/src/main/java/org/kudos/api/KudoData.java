package org.kudos.api;

import lombok.Data;

@Data
public class KudoData {
    private Long id;
    private Long userId;

    public KudoData() {
    }

    public KudoData(Long id, Long userId) {
        this.id = id;
        this.userId = userId;
    }
}
