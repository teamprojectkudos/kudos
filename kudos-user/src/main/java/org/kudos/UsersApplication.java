package org.kudos;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.bson.Document;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.kudos.modules.MainBundle;
import org.kudos.modules.MongoManaged;
import org.kudos.resources.kudosmessage.KudosMessageResource;
import org.kudos.resources.kudosmessage.service.KudosMessageService;
import org.kudos.resources.kudosmessage.service.KudosMessageServiceImpl;
import org.kudos.resources.rabbit.RabbitConsumer;
import org.kudos.resources.users.UserResource;
import org.kudos.resources.users.dao.UserDao;
import org.kudos.resources.users.dao.UserDaoMongo;
import org.kudos.resources.users.service.UserService;
import org.kudos.resources.users.service.UserServiceImpl;
import ru.vyarus.dropwizard.guice.GuiceBundle;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.io.IOException;
import java.util.EnumSet;
import java.util.concurrent.TimeoutException;

public class UsersApplication extends Application<UsersConfiguration> {

    public static void main(final String[] args) throws Exception {
        new UsersApplication().run(args);
    }

    @Override
    public String getName() {
        return "users";
    }

    @Override
    public void initialize(final Bootstrap<UsersConfiguration> bootstrap) {
    }

    @Override
    public void run(final UsersConfiguration configuration,
                    final Environment environment) {
        this.setCORSconfiguration(environment);

        MongoClient mongoClient = new MongoClient(configuration.getMongoHost(), configuration.getMongoPort());
        MongoManaged mongoManaged = new MongoManaged(mongoClient);
        environment.lifecycle().manage(mongoManaged);
        MongoDatabase db = mongoClient.getDatabase(configuration.getMongoDB());
        MongoCollection<Document> collection = db.getCollection(configuration.getCollectionName());

        UserDao userDao = new UserDaoMongo(collection);
        UserService userService = new UserServiceImpl(userDao);
        UserResource userResource = new UserResource(userService);


        KudosMessageService kudosMessageService = new KudosMessageServiceImpl(userDao);
        KudosMessageResource kudosMessageResource = new KudosMessageResource(kudosMessageService);

        environment.jersey().register(userResource);
        environment.jersey().register(kudosMessageResource);
//        RabbitConsumer consumer = null;
//        try {
//            consumer = new RabbitConsumer("kudos-message");
//        } catch (IOException | TimeoutException e) {
//            e.printStackTrace();
//        }
//
//        Thread consumerThread = new Thread(consumer);
//        consumerThread.start();
    }

    private void setCORSconfiguration(Environment environment) {
        FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter(CrossOriginFilter.EXPOSED_HEADERS_PARAM,
                "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Location,username,password,token,admin,message,branch_office");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM,
                "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Location,username,password,token,admin,message,branch_office");
        filter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
    }
}
