//package org.kudos.modules;
//
//import com.google.inject.Provides;
//import org.hibernate.SessionFactory;
//import org.kudos.UsersConfiguration;
//import org.kudos.resources.users.dao.UserDao;
//import org.kudos.resources.users.dao.UserDaoImpl;
//import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;
//
//public class HibernateModule extends DropwizardAwareModule<UsersConfiguration> {
//
//    private final HibernateKudosBundle hibernate;
//
//    public HibernateModule(HibernateKudosBundle hibernate) {
//        this.hibernate = hibernate;
//    }
//
//    @Override
//    protected void configure() {
//
//    }
//
//    @Provides
//    public SessionFactory provideSessionFactory() {
//        return this.hibernate.getSessionFactory();
//    }
//}
